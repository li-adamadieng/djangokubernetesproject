### Test Build on Docker 

`docker build -t djangokubernetesproject .`

`docker run --name djangokubernetesproject -d -p 8000:8000 djangokubernetesproject`

### Redis : 

```

kubectl -n redis exec -it redis-1 -- sh
redis-cli 
auth a-very-complex-password-here
info replication

kubectl -n redis exec -it redis-0 -- sh

```

### PGSQL 

```
psql -h 127.0.0.1 -U amazinguser -d djangokubernetesproject -W
```

### Forwarding :

